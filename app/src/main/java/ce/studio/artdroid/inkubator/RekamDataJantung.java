package ce.studio.artdroid.inkubator;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class RekamDataJantung extends AppCompatActivity {

    LineChart lineChart;

    ArrayList<String> xAXES;
    ArrayList<Entry> yAXESdetakjantung;
    ArrayList<Entry> yAXESberatbadan;

    public static String FEED_URL;

    private static final String TAG = StatusInkubator.class.getSimpleName();
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterKondisiBayi mGridAdapter;
    private ArrayList<GridItemInkubator> mGridData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekam_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lineChart = (LineChart) findViewById(R.id.linechart);

        FEED_URL = "http://";
        FEED_URL += getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("IP","");
        FEED_URL += Config.KONDISIBAYI_URL;
        FEED_URL += "?idinkubator="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idinkubator","");

        Log.e("Rekam Jantung =",FEED_URL);

        xAXES = new ArrayList<>();
        yAXESdetakjantung = new ArrayList<>();
        yAXESberatbadan = new ArrayList<>();

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterKondisiBayi(RekamDataJantung.this, R.layout.list_kondisi_bayi, mGridData);
        mGridView.setAdapter(mGridAdapter);

        new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);

        new AsyncHttpTask().execute();

    }

    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {

            Integer result = 0;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1;
                } else {
                    result = 0;
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);
                String[] xaxes = new String[xAXES.size()];
                for(int i=0; i<xAXES.size();i++){
                    xaxes[i] = xAXES.get(i).toString();
                }

                ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();

                LineDataSet lineDataSet1 = new LineDataSet(yAXESdetakjantung,"Detak Jantung");
                lineDataSet1.setDrawCircles(true);
                lineDataSet1.setColor(Color.BLUE);

                lineDataSets.add(lineDataSet1);

                lineChart.setData(new LineData(xaxes,lineDataSets));

                lineChart.setVisibleXRangeMaximum(65f);

                lineChart.animateY(2000);

                lineChart.setDescription("Status Bayi");
            } else {
//                Toast.makeText(RekamData.this, "Failed to fetch data!", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItemInkubator item;

            yAXESdetakjantung.add(new Entry(0,0));
            xAXES.add(0, "Tanggal");

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String idinkubator = post.optString("idinkubator");
                String namabayi = post.optString("namabayi");
                String jantung = post.optString("detakjantung");
                String beratbadan = post.optString("beratbadan");
                String tanggal = post.optString("tanggal");

                yAXESdetakjantung.add(new Entry(Float.parseFloat(jantung),i+1));
                xAXES.add(i+1, tanggal);

                item = new GridItemInkubator();
                item.setIdinkubator(idinkubator);
                item.setNamabayi(namabayi);
                item.setDetakjantung(jantung);
                item.setBeratbadan(beratbadan);
                item.setTanggal(tanggal);

                mGridData.add(item);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}