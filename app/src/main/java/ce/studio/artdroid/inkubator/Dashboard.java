package ce.studio.artdroid.inkubator;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header= navigationView.getHeaderView(0);
        TextView nama=(TextView) header.findViewById(R.id.namauser);
//        TextView email=(TextView) header.findViewById(R.id.emailuser);
        nama.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("namauser", ""));
//        email.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("email", ""));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tdb) {
            startActivity(new Intent(Dashboard.this,TambahDataBayi.class));
        } else if (id == R.id.nav_ld) {
            startActivity(new Intent(Dashboard.this,LihatDataBayi.class));
        } else if (id == R.id.nav_ss) {
            startActivity(new Intent(Dashboard.this,ListInkubator.class));
        } else if (id == R.id.nav_lkb) {
            startActivity(new Intent(Dashboard.this,ListStatusInkubator.class));
        } else if (id == R.id.nav_k) {
            startActivity(new Intent(Dashboard.this, Login.class));
            finish();
            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
            editor.putString("statlogin", "0");
            editor.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
