package ce.studio.artdroid.inkubator;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TambahDataBayi extends AppCompatActivity {

    public static String FEED_URL;

    private DatePickerDialog DatePickerDialog;

    private SimpleDateFormat dateFormatter;

    private static final String TAG_RESULT = "result";
    private static final String TAG_TAMBAH = "tambah";

    static boolean a=false;

    JSONArray rs = null;

    EditText tambahnamainkubator,tambahnamabayi,tambahalamat,tambahberatbadan,tambahtanggallahir;

    Button btnsimpan,btnbatal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_data_bayi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        tambahnamainkubator = (EditText) findViewById(R.id.tambahnamainkubator);
        tambahnamabayi = (EditText) findViewById(R.id.tambahnamabayi);
        tambahalamat = (EditText) findViewById(R.id.tambahalamat);
        tambahberatbadan = (EditText) findViewById(R.id.tambahberatbadan);
        tambahtanggallahir = (EditText) findViewById(R.id.tambahtanggallahir);

        btnsimpan = (Button) findViewById(R.id.btnsimpan);
        btnbatal = (Button) findViewById(R.id.btnbatal);

        setDateTimeField();

        tambahtanggallahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.show();
            }
        });

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TambahBayi().execute();
            }
        });

        btnbatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tambahtanggallahir.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private class TambahBayi extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.TAMBAHDATABAYI_URL;
            FEED_URL += "?namainkubator="+tambahnamainkubator.getText().toString().replace(" ","%20");
            FEED_URL += "&namabayi="+tambahnamabayi.getText().toString().replace(" ","%20");
            FEED_URL += "&alamat="+tambahalamat.getText().toString().replace(" ","%20");
            FEED_URL += "&beratbadan="+tambahberatbadan.getText().toString().replace(" ","%20");
            FEED_URL += "&tanggallahir="+tambahtanggallahir.getText().toString().replace(" ","%20");

            Log.e("Tambah Data Bayi =",FEED_URL);

            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String tambah = a.getString(TAG_TAMBAH);
                        if (tambah.equals("0")){
                            Toast.makeText(TambahDataBayi.this, "Gagal menambah data bayi!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(TambahDataBayi.this, "Sukses menambah data bayi!", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }

}
