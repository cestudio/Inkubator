package ce.studio.artdroid.inkubator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class KadarOksigen extends AppCompatActivity {

    TextView detakjantung,kadaroksigen,namainkubator;

    public static String FEED_URL;

    private static final String TAG_RESULT = "result";
    private static final String TAG_DETAKJANTUNG = "detakjantung";
    private static final String TAG_KADAROKSIGEN = "kadaroksigen";
    private static final String TAG_NAMABAYI = "namabayi";

    static boolean a=false;

    JSONArray rs = null;
    String myJSON="";
    boolean close=false;

    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kadar_oksigen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        close=false;

        mp = MediaPlayer.create(KadarOksigen.this, R.raw.alarm);

        detakjantung = (TextView) findViewById(R.id.detakjantung);
        kadaroksigen = (TextView) findViewById(R.id.kadaroksigen);
        namainkubator = (TextView) findViewById(R.id.namainkubator);

        namainkubator.setText('"'+getIntent().getStringExtra("namainkubator")+'"');

        new monitoring().execute();
    }

    public void onBackPressed() {
        mp.stop();
        finish();
        close=true;
    }

    private class monitoring extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.MONITORING_URL;
            FEED_URL += "?idinkubator="+getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("idinkubator","");
            JSONParser jParser = new JSONParser();
            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strdetakjantung = a.getString(TAG_DETAKJANTUNG);
                        String strkadaroksigen = a.getString(TAG_KADAROKSIGEN);
                        String strnamabayi = a.getString(TAG_NAMABAYI);

                        detakjantung.setText(strdetakjantung);
                        kadaroksigen.setText(strkadaroksigen);
                        namainkubator.setText('"'+strnamabayi+'"');

                        if(Integer.parseInt(strdetakjantung)<120){
                            mp.start();
                        }
                        else if(Integer.parseInt(strdetakjantung)>160){
                            mp.start();
                        }
                        else if(Integer.parseInt(strkadaroksigen)<10){
                            mp.start();
                        }
                        else{
                            mp.stop();
                            mp = MediaPlayer.create(KadarOksigen.this, R.raw.alarm);
                        }

                    }
                    myJSON=String.valueOf(rs);
                    if(close){

                    }
                    else{
                        new monitoring().execute();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}
