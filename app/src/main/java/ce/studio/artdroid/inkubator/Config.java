package ce.studio.artdroid.inkubator;

public class Config {

    public static String LOGIN_URL="/inkubator/login.php";
    public static String LISTBAYI_URL="/inkubator/listdatabayi.php";
    public static String TAMBAHDATABAYI_URL="/inkubator/tambahdatabayi.php";
    public static String UBAHSUHU_URL="/inkubator/ubahsuhu.php";
    public static String LISTINKUBATOR_URL="/inkubator/listinkubator.php";
    public static String STATUSSUHUKELEMBABAN_URL="/inkubator/statussuhukelembaban.php";

    public static String KONDISIBAYI_URL="/inkubator/kondisibayi.php";
    public static String MONITORING_URL="/inkubator/monitoring.php";

}
