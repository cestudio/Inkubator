package ce.studio.artdroid.inkubator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    public static String FEED_URL;

    ImageView logo;

    private static final String TAG_RESULT = "result";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_IDUSER = "iduser";
    private static final String TAG_NAMAUSER = "namauser";

    static boolean a=false;

    JSONArray rs = null;

    EditText username,password;

    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        logo = (ImageView) findViewById(R.id.loginlogo);
        login = (Button) findViewById(R.id.btnloginlogin);
        username = (EditText) findViewById(R.id.loginusername);
        password = (EditText) findViewById(R.id.loginpassword);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SendLogin().execute();
//                if(username.getText().toString().equals("admin")&&password.getText().toString().equals("admin")){
//                    startActivity(new Intent(Login.this,Dashboard.class));
//                    finish();
//                    SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
//                    editor.putString("statlogin", "1");
//                    editor.putString("namauser", "Admin");
//                    editor.commit();
//                }
//                else{
//                    Toast.makeText(Login.this, "Username atau password salah!", Toast.LENGTH_SHORT).show();
//                }
            }
        });

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IP ip = new IP(Login.this);
                ip.show();
            }
        });
    }

    private class SendLogin extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.LOGIN_URL;
            FEED_URL += "?username="+username.getText().toString();
            FEED_URL += "&password="+password.getText().toString();
            Log.e("Login = ",FEED_URL);
            String URL = FEED_URL;

            JSONParser jParser = new JSONParser();
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String login = a.getString(TAG_LOGIN);

                        if (login.equals("0")){
                            Toast.makeText(Login.this, "Username/Password Anda Salah!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            String iduser = a.getString(TAG_IDUSER);
                            String namauser = a.getString(TAG_NAMAUSER);

                            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                            editor.putString("statlogin", "1");
                            editor.putString("iduser", iduser);
                            editor.putString("namauser", namauser);
                            editor.commit();
                            startActivity(new Intent(Login.this,Dashboard.class));
                            finish();
                        }

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}
