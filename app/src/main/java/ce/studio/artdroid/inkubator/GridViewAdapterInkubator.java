package ce.studio.artdroid.inkubator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class GridViewAdapterInkubator extends ArrayAdapter<GridItemInkubator> {

    GridItemInkubator item;

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemInkubator> mGridData = new ArrayList<GridItemInkubator>();

    public GridViewAdapterInkubator(Context mContext, int layoutResourceId, ArrayList<GridItemInkubator> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemInkubator> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idinkubator = (TextView) row.findViewById(R.id.idinkubator);
            holder.namainkubator = (TextView) row.findViewById(R.id.namainkubator);
            holder.suhu = (TextView) row.findViewById(R.id.suhu);
            holder.kelembaban = (TextView) row.findViewById(R.id.kelembaban);
            holder.tanggal = (TextView) row.findViewById(R.id.tanggal);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        item = mGridData.get(position);
        holder.idinkubator.setText(Html.fromHtml(item.getIdinkubator()));
        holder.namainkubator.setText(Html.fromHtml(item.getNamainkubator()));
        holder.suhu.setText("Suhu saat ini "+Html.fromHtml(item.getSuhu())+"ºC");
        holder.kelembaban.setText("Kelembaban "+Html.fromHtml(item.getKelembaban())+" %");
        holder.tanggal.setText(Html.fromHtml(item.getTanggal()));

        return row;
    }

    static class ViewHolder {
        TextView idinkubator,namainkubator,suhu,kelembaban,tanggal;

    }
}