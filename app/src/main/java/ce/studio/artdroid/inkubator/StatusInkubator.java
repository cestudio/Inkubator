package ce.studio.artdroid.inkubator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class StatusInkubator extends AppCompatActivity {

    TextView suhu,kelembaban,pintu,suara,namainkubator;

    public static String FEED_URL;

    private static final String TAG_RESULT = "result";
    private static final String TAG_SUHU = "suhu";
    private static final String TAG_KELEMBABAN = "kelembaban";
    private static final String TAG_PINTU= "pintu";
    private static final String TAG_SUARA = "suara";
    private static final String TAG_NAMABAYI = "namabayi";

    static boolean a=false;

    JSONArray rs = null;
    String myJSON="";
    boolean close=false;

    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_inkubator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        close=false;

        mp = MediaPlayer.create(StatusInkubator.this, R.raw.alarm);

        suhu = (TextView) findViewById(R.id.suhu);
        kelembaban = (TextView) findViewById(R.id.kelembaban);
        pintu = (TextView) findViewById(R.id.pintu);
        suara = (TextView) findViewById(R.id.suara);
        namainkubator = (TextView) findViewById(R.id.namainkubator);

        namainkubator.setText('"'+getIntent().getStringExtra("namainkubator")+'"');

        new monitoring().execute();
    }

    public void onBackPressed() {
        mp.stop();
        finish();
        close=true;
    }

    private class monitoring extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.MONITORING_URL;
            FEED_URL += "?idinkubator="+getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("idinkubator","");
            JSONParser jParser = new JSONParser();
            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);

                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String strsuhu = a.getString(TAG_SUHU);
                        String strkelembaban = a.getString(TAG_KELEMBABAN);
                        String strpintu = a.getString(TAG_PINTU);
                        String strsuara = a.getString(TAG_SUARA);
                        String strnamabayi = a.getString(TAG_NAMABAYI);

                        suhu.setText(strsuhu);
                        kelembaban.setText(strkelembaban);
                        pintu.setText(strpintu);
                        suara.setText(strsuara);
                        namainkubator.setText('"'+strnamabayi+'"');

                        if(Integer.parseInt(strsuhu)<30){
                            mp.start();
                        }
                        else if(Integer.parseInt(strsuhu)>37){
                            mp.start();
                        }
                        else if(Integer.parseInt(strkelembaban)<41){
                            mp.start();
                        }
                        else if(Integer.parseInt(strkelembaban)>60){
                            mp.start();
                        }
                        else if(Integer.parseInt(strpintu)==0){
                            mp.start();
                        }
                        else if(Integer.parseInt(strsuara)==0){
                            mp.start();
                        }
                        else{
                            mp.stop();
                            mp = MediaPlayer.create(StatusInkubator.this, R.raw.alarm);
                        }

                    }
                    myJSON=String.valueOf(rs);
                    if(close){

                    }
                    else{
                        new monitoring().execute();
                    }

                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}
