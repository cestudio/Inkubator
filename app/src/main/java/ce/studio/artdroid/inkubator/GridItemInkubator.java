package ce.studio.artdroid.inkubator;

/**
 * Created by Artha on 2/2/2017.
 */
public class GridItemInkubator {

    String idinkubator,namainkubator,suhu,kelembaban,tanggal,detakjantung,beratbadan,kadaroksigen,namabayi;

    public String getDetakjantung() {
        return detakjantung;
    }

    public void setDetakjantung(String detakjantung) {
        this.detakjantung = detakjantung;
    }

    public String getBeratbadan() {
        return beratbadan;
    }

    public void setBeratbadan(String beratbadan) {
        this.beratbadan = beratbadan;
    }

    public String getKadaroksigen() {
        return kadaroksigen;
    }

    public void setKadaroksigen(String kadaroksigen) {
        this.kadaroksigen = kadaroksigen;
    }

    public String getNamabayi() {
        return namabayi;
    }

    public void setNamabayi(String namabayi) {
        this.namabayi = namabayi;
    }

    public String getIdinkubator() {

        return idinkubator;


    }

    public void setIdinkubator(String idinkubator) {

        this.idinkubator = idinkubator;

    }

    public String getNamainkubator() {

        return namainkubator;

    }

    public void setNamainkubator(String namainkubator) {

        this.namainkubator = namainkubator;

    }

    public String getKelembaban() {

        return kelembaban;

    }

    public void setKelembaban(String kelembaban) {

        this.kelembaban = kelembaban;

    }

    public String getTanggal() {

        return tanggal;

    }

    public void setTanggal(String tanggal) {

        this.tanggal = tanggal;

    }

    public String getSuhu() {

        return suhu;

    }

    public void setSuhu(String suhu) {

        this.suhu = suhu;

    }
}