package ce.studio.artdroid.inkubator;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class LihatDataBayi extends AppCompatActivity {

    public static String FEED_URL;

    String myJSON;

    JSONArray data = null;

    ArrayList<HashMap<String, String>> bayiList;

    ListView list;

    private static final String TAG_RESULTS="result";
    private static final String TAG_IDBAYI = "idbayi";
    private static final String TAG_NAMABAYI = "namabayi";
    private static final String TAG_IDINKUBATOR = "idinkubator";
    private static final String TAG_NOMORINKUBATOR = "nomorinkubator";
    private static final String TAG_BERATBADAN = "beratbadan";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_data_bayi);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        list = (ListView) findViewById(R.id.listbayi);

        bayiList = new ArrayList<HashMap<String,String>>();

        getData();
    }

    public void getData(){
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
                FEED_URL = "http://";
                FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
                FEED_URL += Config.LISTBAYI_URL;
                HttpPost httppost = new HttpPost(FEED_URL);

                // Depends on your web service
                httppost.setHeader("Content-type", "application/json");

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();
                    // json is UTF-8 by default
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (Exception e) {
                    // Oops
                }
                finally {
                    try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result){
                myJSON=result;
                showList();

            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    protected void showList(){
        try {
            JSONObject jsonObj = new JSONObject(myJSON);
            data = jsonObj.getJSONArray(TAG_RESULTS);

            for(int i=0;i<data.length();i++){
                JSONObject c = data.getJSONObject(i);
                String idbayi = c.getString(TAG_IDBAYI);
                String namabayi = "Nama Bayi : " + c.getString(TAG_NAMABAYI);
                String nomorinkubator = "No Inkubator : " + c.getString(TAG_IDINKUBATOR);
                String idinkubator = c.getString(TAG_IDINKUBATOR);
                String beratbadan = "Berat Badan : " + c.getString(TAG_BERATBADAN);

                HashMap<String,String> persons = new HashMap<String,String>();

                persons.put(TAG_IDBAYI, idbayi);
                persons.put(TAG_NAMABAYI, namabayi);
                persons.put(TAG_NOMORINKUBATOR, nomorinkubator);
                persons.put(TAG_IDINKUBATOR, idinkubator);
                persons.put(TAG_BERATBADAN, beratbadan);

                bayiList.add(persons);
            }

            ListAdapter adapter = new SimpleAdapter(
                    this, bayiList, R.layout.list_data_bayi,
                    new String[]{TAG_IDBAYI, TAG_NAMABAYI, TAG_IDINKUBATOR, TAG_NOMORINKUBATOR, TAG_BERATBADAN},
                    new int[]{R.id.idbayi, R.id.namabayi, R.id.idinkubator, R.id.nomorinkubator, R.id.beratbadan}
            );
            list.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
