package ce.studio.artdroid.inkubator;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class GridViewAdapterKondisiBayi extends ArrayAdapter<GridItemInkubator> {

    GridItemInkubator item;

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemInkubator> mGridData = new ArrayList<GridItemInkubator>();

    public GridViewAdapterKondisiBayi(Context mContext, int layoutResourceId, ArrayList<GridItemInkubator> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemInkubator> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idinkubator = (TextView) row.findViewById(R.id.idinkubator);
            holder.namabayi = (TextView) row.findViewById(R.id.namabayi);
            holder.detakjantung = (TextView) row.findViewById(R.id.detakjantung);
            holder.beratbadan = (TextView) row.findViewById(R.id.beratbadan);
            holder.tanggal = (TextView) row.findViewById(R.id.tanggal);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        item = mGridData.get(position);
        holder.idinkubator.setText(Html.fromHtml(item.getIdinkubator()));
        holder.namabayi.setText(Html.fromHtml(item.getNamabayi()));
        holder.detakjantung.setText("Detak Jantung : "+Html.fromHtml(item.getDetakjantung())+" bpm");
        holder.beratbadan.setText("Berat Badan : "+Html.fromHtml(item.getBeratbadan())+" Kg");
        holder.tanggal.setText(Html.fromHtml(item.getTanggal()));

        return row;
    }

    static class ViewHolder {
        TextView idinkubator,namabayi,detakjantung,beratbadan,tanggal;

    }
}