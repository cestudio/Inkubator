package ce.studio.artdroid.inkubator;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

public class SetSuhu extends AppCompatActivity {

    public static String FEED_URL;

    private static final String TAG_RESULT = "result";
    private static final String TAG_UBAH = "ubah";

    static boolean a=false;

    JSONArray rs = null;

    EditText setsuhumin,setsuhumax,setsuhukelembabanmin,setsuhukelembabanmax;

    Button btnsimpan,btnbatal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_suhu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setsuhumin = (EditText) findViewById(R.id.setsuhumin);
        setsuhumax = (EditText) findViewById(R.id.setsuhumax);
        setsuhukelembabanmin = (EditText) findViewById(R.id.setsuhukelembabanmin);
        setsuhukelembabanmax = (EditText) findViewById(R.id.setsuhukelembabanmax);

        btnsimpan = (Button) findViewById(R.id.btnsimpan);
        btnbatal = (Button) findViewById(R.id.btnbatal);

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new UpdateSuhu().execute();
            }
        });

        btnbatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private class UpdateSuhu extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.UBAHSUHU_URL;
            FEED_URL += "?idinkubator="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idinkubator","");
            FEED_URL += "&setsuhumin="+setsuhumin.getText().toString();
            FEED_URL += "&setsuhumax="+setsuhumax.getText().toString();
            FEED_URL += "&setkelembabanmin="+setsuhukelembabanmin.getText().toString();
            FEED_URL += "&setkelembabanmax="+setsuhukelembabanmax.getText().toString();

            Log.e("Ubah Suhu =",FEED_URL);

            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(FEED_URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String ubah = a.getString(TAG_UBAH);
                        if (ubah.equals("0")){
                            Toast.makeText(SetSuhu.this, "Gagal mengubah suhu!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(SetSuhu.this, "Sukses mengubah suhu!", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {

                Toast.makeText(getApplicationContext(), "Server Maintenance", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}
